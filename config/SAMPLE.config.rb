# Customize this file, and then rename it to config.rb

set :application, "My test site"
set :repository,  "git@github.com:hilja/test200000.git"
set :scm, :git
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

# Using Git Submodules?
set :git_enable_submodules, 1

# This should be the same as :deploy_to in production.rb
set :production_deploy_to, '/var/www/example.com/public_html'

# The domain name used for your staging environment
set :staging_domain, 'staging.example.com'

# Database
# Set the values for host, user, pass, and name for both production and staging.
set :wpdb do
    {
        :production => {
            :host     => 'localhost',
            :user     => 'example',
            :password => 'xxxxxxxxx',
            :name     => 'example',
        },
        :staging => {
            :host     => 'localhost',
            :user     => 'example',
            :password => 'xxxxxxxx',
            :name     => 'example',
        }
    }
end

# You're not done! You must also configure production.rb and staging.rb
